![Expensiveapp Intro picture](classic-close-up-draw-expensive-372748.jpg)

# ExpensiveApp (Flutter)

## What was created

> An app that is used to keep track of expenses, by date, week.

## Flutter version

Flutter 1.12.13+hotfix.5

## Dart version

Dart 2.7.0

## Reflections

- Stacks, and FractionallySizedBoxes created for charts
- Having a calendar for the app, which is used for keeping track of dates, day of expenses.
- Sizing of the text to fit certain small areas


## By

> [Khairul A K](mailto:khairulkulma@gmail.com)

![Expensiveapp End Banner](antique-bills-business-cash-210600.jpg)
